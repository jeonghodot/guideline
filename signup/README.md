<img src="images/kt_title.png" alt="login" width="350">

> 회원 가입 가이드라인<br>
> KT 융합기술원 / AI연구소 / AI Tech Center / AI Device & Eco / SW QA & 거버넌스팀

## 1. KT Agile R&D 개발자 포털 사이트 접속
- http://agilernd.kt.co.kr/
<img src="images/login.png" alt="login" width="350">

## 2. 회원 가입 선택 및 회원 정보 입력
- 사번 입력
- 비밀번호 입력
- Captcha 코드 입력
- 개인정보 활용 동의
- 정보보호 서약 동의
<img src="images/step1.png" alt="login" width="500">
<br><br>

- 사용자 정보 확인 메시지 확인
<img src="images/msg1.png" alt="login" width="300">

## 3. 회원 정보 확인
- 개인정보 확인
    - 사번
    - 이름
    - 소속
    - E-MAIL
<img src="images/step2.png" alt="login" width="500">

## 4. 회원 등록 요청
- 승인자 선택
    - 직접 입력
        - 승인자 E-MAIL주소 직접 입력
    - 승인 가능 직원 선택
        - 승인 권한이 있는 목록에서 선택
- Captcha 코드 입력
- 사용자 인증번호 (제한시간: 10분)
    - 가입자 본인 E-MAIL로 받은 인증번호 입력
- 승인자 인증번호
    - 선택한 승인자 E-MAIL로 받은 인증번호 입력

<img src="images/step3.png" alt="login" width="500">

<img src="images/step3-2.png" alt="login" width="500">
<br><br>

- 인증번호 발송 메시지 확인
<img src="images/msg2.png" alt="login" width="300">

## 5. KT Agile R&D 개발자 포털 사이트 접속 및 로그인
- http://agilernd.kt.co.kr/
- 사번 입력
- 비밀번호 입력
- E-MAIL 인증 (제한시간: 10분)

<img src="images/msgLogin.png" alt="login" width="300"> <br>

- 인증번호 발송 메시지 확인
- 인증번호 입력
- 로그인

<img src="images/portal.png" alt="login" width="350">


